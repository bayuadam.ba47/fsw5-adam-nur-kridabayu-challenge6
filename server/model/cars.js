const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    plate: String,
    manufacture: {
        type: String,
        required: true,
    },
    model: String,
    foto: String,
    rentPerday: {
        type: String,
        required: true,
    },
    capacity: String,
    description: String,
    transmission: String,
    type : String,
    year: String,
    size: String,
    options: String,
    specs: String,
    availableAt: String,
    typeDriver: String,
    available: String,
    createdBy: String,
    updatedBy: String,
},
    { timestamps: true }
)

const Cardb = mongoose.model('tbMobil', schema);

module.exports = Cardb;