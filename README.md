## Getting Started

Untuk menjalankan development server, tinggal jalanin salah satu script di package.json.

```sh
npm start
```

## Database Management

Di dalam repository ini sudah terdapat beberapa script yang dapat digunakan dalam memanage database, yaitu:

- `npm run db:seed-users` digunakan untuk melakukan seeding
- `npm run db:seed-cars` digunakan untuk melakukan seeding

## Documentation

Berikut link documentation menggunakan Postman https://bit.ly/FSW5-Kelompok1-CH6


## Endpoints

Di dalam repository ini, terdapat beberapa endpoint yaitu

##### POST `http://localhost:3000/api/users/register-super-admin`

##### POST `http://localhost:3000/api/users/register-admin`

##### POST `http://localhost:3000/api/users/register-member`

##### POST `http://localhost:3000/api/users/login-super-admin` 

##### POST `http://localhost:3000/api/users/login-admin`

##### POST `http://localhost:3000/api/users/login-member`

##### GET `http://localhost:3000/api/users/getuser`

##### GET `http://localhost:3000/api/cars`

##### GET `http://localhost:3000/api/cars/:id`

##### DELETE `http://localhost:3000/api/cars/:id`

##### POST `http://localhost:3000/api/cars`

##### PUT `http://localhost:3000/api/cars/:id`



